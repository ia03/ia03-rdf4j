# IA03 - TD1 : Apprendre à manipuler RDF4J 

[RDF4J](https://rdf4j.org/) est un framework JAVA qui permet de réaliser la plupart des opréations dont tout développeur manipulant des données RDF aura besoin à un moment ou à un autre.
[RDF4J](https://rdf4j.org/) est un projet de la fondation Eclipse.

Des alternatives à [RDF4J](https://rdf4j.org/) existent, la plus connue d'entre elle est certainement [JENA](https://jena.apache.org/) de la Fondation Apache.
D'autres frameworks existent dans d'autres langages, en Python notamment avec [RDFLib](https://github.com/RDFLib/rdflib).

Nous allons utiliser [RDF4J](https://rdf4j.org/) dans ce TD car le triple store que nous allons interroger est 100% compatible avec RDF4J mais pas avec JENA. 

Néanmoins, si vous préférez utiliser JENA ou même RDFLib, libre à vous !

## Etape 0 : Créer un projet Maven depuis le template  
```shell script
mvn archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4
```

Quand le mode interactif vous demande de renseigner les informations sur le projet, mettre les valeurs suivantes :

```
'groupId': fr.utc.ia03
'artifactId': ia03-rdf4j
'version' 1.0-SNAPSHOT (par défaut)
'package' fr.utc.ia03 (par défaut)
```

## Etape 1 : Apprendre à utiliser Maven
```shell script
# supprimer le répertoire target
mvn clean

# installer les dépendances dans le cache local
mvn install

# compiler le code source
mvn compile

# compile le code source et constuit le jar de l'application (sans les dépendances) dans le répertoire target
mvn package

# Exécuter les tests
mvn test

# Il est possible de combiner les commandes
mvn clean install package
```

## Etape 2 : Ajouter RDF4J au projet
Editez le fichier `pom.xml` et rajoutez les dépendances RDF4J suivantes :

```xml
<dependencies>
    <dependency>
        <groupId>org.eclipse.rdf4j</groupId>
        <artifactId>rdf4j-runtime</artifactId>
        <version>3.4.4</version>
        <type>pom</type>
    </dependency>
    <dependency>
        <groupId>org.eclipse.rdf4j</groupId>
        <artifactId>rdf4j-query</artifactId>
        <version>3.4.4</version>
    </dependency>
    ...
</dependencies>
```

## Etape 3 : Parsing d'un jeu de données JSON en Java
Téléchargez le fichier [restaurants.zip](https://drive.google.com/file/d/1j69UaYV9DMDnD5pmR2zseHGJaldNoslh/view?usp=sharing).
Décompressez le.

A l'aide de la librairie [Jackson Databind](https://github.com/FasterXML/jackson-databind) (incluse dans RDF4J), implémentez un parser JSON qui permet de charger les données des restaurants dans des classes Java, de type "Java Bean".

Le parser devra implémenter l'interface suivante :

```java
package fr.utc.ia03.api;

import com.fasterxml.jackson.databind.MappingIterator;

import java.io.IOException;
import java.io.InputStream;

public interface GenericParser<T> {
    MappingIterator<T> parse(String filepath) throws IOException;
    MappingIterator<T> parse(InputStream is) throws IOException;
}
```
Dans les deux cas, les méthodes devront renvoyer un [MappingIterator](https://github.com/FasterXML/jackson-databind/blob/master/src/main/java/com/fasterxml/jackson/databind/MappingIterator.java).

Afin de valider que le parser fonctionne bien, vous devrez proposer au moins un test unitaire qui vérifie que le nombre de restaurants parsé est bien égal à `25359`.
Pour cela vous pouvez vous inspirer du test unitaire suivant.

```java
package fr.utc.ia03;

import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.databind.MappingIterator;
import com.google.common.collect.ImmutableList;
import fr.utc.ia03.api.RestaurantParser;
import fr.utc.ia03.models.Restaurant;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Logger;

public class RestaurantParserSpec {

    private final static Logger logger = Logger.getGlobal();
    private RestaurantParser parser = new RestaurantParser();

    @Test
    public void shouldParseFile() {
        try {
            MappingIterator<Restaurant> it = parser.parse("./data/restaurants.json");
            List<Restaurant> restos = ImmutableList.copyOf(it);
            assertTrue(restos.size() == 25359);
        } catch (IOException e) {
            logger.severe("Something went wrong while parsing the file");
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void shouldParseStream() {
        try {
            Path p = Paths.get("./data/restaurants.json");
            logger.info("Parsing file : "+p.toFile().getAbsolutePath());
            InputStream is = Files.newInputStream(p);
            MappingIterator<Restaurant> it = parser.parse(is);
            List<Restaurant> restos = ImmutableList.copyOf(it);
            assertTrue(restos.size() == 25359);
            is.close();
        } catch (IOException e) {
            logger.severe("Something went wrong while parsing the file");
            e.printStackTrace();
            assertTrue(false);
        }
    }
}
```

## Etape 4 : Charger les données JSON dans un "Model" RDF4J

Dans cette étape, nous allons ajouter des méthodes aux Beans créés précédemment afin leur permettre, à terme, de se sérialiser en RDF.

Pour cela nous allons surcharger l'interface `Serializable` de Java en lui ajoutant quelques méthodes.

```java
package fr.utc.ia03.api;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.util.ModelBuilder;

import java.io.Serializable;

public interface RDFSerializable extends Serializable {

    /**
     * Méthode qui renvoie une {@link org.eclipse.rdf4j.model.Resource} qui contient l'URI du sujet.
     * @return La Resource qui décrit le sujet.
     */
    Resource getSubject();

    /**
     * Méthode qui renvoie l'{@link org.eclipse.rdf4j.model.IRI} du sujet.
     * @return l'IRI du sujet.
     */
    IRI getRDFType();

    /**
     * Méthode qui renvoie l'{@link org.eclipse.rdf4j.model.IRI} du sujet sous forme de String.
     * @return une chaine de caractères contenant l'IRI du sujet.
     */
    String getIRI();

    /**
     * Méthode qui ajoute les prédicats du sujet dans un builder passé en paramètre.
     * Les prédicats doivent être conformes à un modèle RDFS/OWL préalablement défini.
     * Dans la mesure du possible il faut utiliser les vocabulaires existants pour éviter de rajouter des
     * relations qui existent déjà.
     * @return le builder reçu en paramètre complété avec les données du sujet.
     */
    ModelBuilder build(ModelBuilder builder);
}
```

Pour générer un [Model](https://github.com/eclipse/rdf4j/blob/master/core/model/src/main/java/org/eclipse/rdf4j/model/Model.java) à partir d'une classe Java, le plus simple est d'utiliser le [ModelBuilder](https://github.com/eclipse/rdf4j/blob/master/core/model/src/main/java/org/eclipse/rdf4j/model/util/ModelBuilder.java).

La gestion des namespaces et des préfixes est importante, il vous faudra définir un namespace spécifique pour l'exercice.
Pour cela vous pouvez vous inspirer des [Namespaces déjà présents dans RDF4J](https://github.com/eclipse/rdf4j/tree/master/core/model/src/main/java/org/eclipse/rdf4j/model/vocabulary).

Pour la sérialisation RDF des objets `Restaurant` et `Adresse`, vous devrez utiliser les vocabulaires :
* [FOAF](https://github.com/eclipse/rdf4j/blob/master/core/model/src/main/java/org/eclipse/rdf4j/model/vocabulary/FOAF.java) pour les informations concernant le restaurant
* [VCARD](https://github.com/eclipse/rdf4j/blob/master/core/model/src/main/java/org/eclipse/rdf4j/model/vocabulary/VCARD4.java) pour les informations concernant l'adresse et la localisation du restaurant
* [WGS84](https://github.com/eclipse/rdf4j/blob/master/core/model/src/main/java/org/eclipse/rdf4j/model/vocabulary/WGS84.java) pour les coordonnées géographiques, respectivement `longitude` et `latitude` présentes dans le champ `coord` du fichier JSON.


## Etape 5 : Sérialiser les données RDF dans un fichier

RDF4J dispose d'un outil très puissant pour le parsing et la serialisation de données RDF sous toutes ses formes.

[Voir la doc de RIO](https://rdf4j.org/documentation/programming/rio/).

A partir des objets Model que vous avez généré dans l'étape 4, vous devez à présent écrire ces données dans un ou plusieurs fichiers en utilisant Rio.

Les fichiers devront être sérialisés dans les formats TriG et JsonLD. 